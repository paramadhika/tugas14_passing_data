package com.example.login__register_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.login.*
import com.example.login__register_project.user.SetUser

class LoginActivity : AppCompatActivity() {

    private lateinit var dataUsername : EditText
    private lateinit var dataPassword : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        dataUsername = findViewById(R.id.loginuser)
        dataPassword = findViewById(R.id.pass)
        pass.transformationMethod = PasswordTransformationMethod.getInstance()

        backfromlogin.setOnClickListener{
            finish()
        }
        signupfromlgn.setOnClickListener{
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
        signinbtn.setOnClickListener{
            if (intent.extras == null){
                Toast.makeText(this, "Harap Melakukan Registrasi Terlebih Dahulu!", Toast.LENGTH_LONG).show()
                val intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)
            }
            else if (intent.extras != null){
                val bundle = intent.extras
                if (dataUsername.text.toString() != bundle!!.getString("username")
                    || dataPassword.text.toString() != bundle!!.getString("pass")){
                    Toast.makeText(this, "Username atau Password Salah!", Toast.LENGTH_LONG).show()
                }
                else{
                    Toast.makeText(this, "Berhasil Login!", Toast.LENGTH_LONG).show()
                    val intent = Intent(this, SetUser::class.java)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }
        }
        googlebtn.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }
    }
}