package com.example.login__register_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.login__register_project.databinding.ActivityDescriptionBinding
import kotlinx.android.synthetic.main.activity_description.*

class DescriptionActivity : AppCompatActivity(), dgtArtClickListerner {
    private lateinit var binding: ActivityDescriptionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDescriptionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        backfromdesc.setOnClickListener{
            finish()
        }

        val dgtartID = intent.getIntExtra(DGTART_ID_EXTRA, -1)
        val dgtart = dgtartFromID(dgtartID)

        if(dgtart != null){
            binding.cover.setImageResource(dgtart.dgtimage)
            binding.title.text = dgtart.dgttitle
            binding.desc.text = dgtart.dgtdesc
            binding.Dgtartist.text = dgtart.dgtartists
        }

        val descriptionActivity = this
        binding.recycleView.apply {
            layoutManager = GridLayoutManager(applicationContext, 2)
            adapter = DigitalArtAdapter(dgtArtList, descriptionActivity)
        }
    }

    private fun dgtartFromID(dgtartID: Int): DigitalArt?
    {
        for (dgtart in dgtArtList){
            if(dgtart.id == dgtartID)
                return dgtart
        }
        return null
    }

    override fun onClick(digitalArt: DigitalArt) {
        val intent = Intent(applicationContext, DescriptionActivity::class.java)
        intent.putExtra(DGTART_ID_EXTRA, digitalArt.id)
        startActivity(intent)
    }
}