package com.example.login__register_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.login__register_project.user.UserActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {
    private lateinit var inputUsername : EditText
    private lateinit var inputPass : EditText
    private lateinit var konfirmPass : EditText
    private lateinit var signUp : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        inputUsername = findViewById(R.id.username)
        inputPass = findViewById(R.id.pass1)
        konfirmPass = findViewById(R.id.pass2)
        signUp = findViewById(R.id.signUpbtn)

        signUp.setOnClickListener {

            intent.putExtra("username", inputUsername.text.toString())
            intent.putExtra("pass", inputPass.text.toString())
            intent.putExtra("confpass", konfirmPass.text.toString())

            if (inputUsername.text.toString() == ""
                || inputPass.text.toString() == ""
                || konfirmPass.text.toString() == ""){
                Toast.makeText(this, "Harap Lengkapi Data Registrasi!", Toast.LENGTH_LONG).show()
            }
            else if (intent.getStringExtra("pass") != intent.getStringExtra("confpass")){
                Toast.makeText(this, "Password Tidak Sesuai!", Toast.LENGTH_LONG).show()
            }
            else{
                val bundle = Bundle()
                bundle.putString("username", inputUsername.text.toString())
                bundle.putString("pass", inputPass.text.toString())
                val intent = Intent(this, LoginActivity::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }

        pass1.transformationMethod = PasswordTransformationMethod.getInstance()
        pass2.transformationMethod = PasswordTransformationMethod.getInstance()

        backfromregis.setOnClickListener{
            finish()
        }

        signinfromrgs.setOnClickListener{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }
}