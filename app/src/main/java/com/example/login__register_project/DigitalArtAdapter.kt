package com.example.login__register_project

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.login__register_project.databinding.ItemviewBinding

class DigitalArtAdapter(private val digitalArt : List<DigitalArt>, private val clickListerner: dgtArtClickListerner)
    : RecyclerView.Adapter<DigitalArtView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DigitalArtView {
        val asal = LayoutInflater.from(parent.context)
        val binding = ItemviewBinding.inflate(asal, parent, false)
        return DigitalArtView(binding, clickListerner)
    }

    override fun onBindViewHolder(holder: DigitalArtView, position: Int) {
        holder.binddgtArt(digitalArt[position])
    }

    override fun getItemCount(): Int = digitalArt.size
}