package com.example.login__register_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.login__register_project.databinding.ActivityHomeBinding
import com.example.login__register_project.databinding.ActivityMainBinding
import com.example.login__register_project.databinding.ItemviewBinding

class HomeActivity : AppCompatActivity(), dgtArtClickListerner {
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getDigitalArt()

        val homeActivity = this
        binding.recycleView.apply {
            layoutManager = GridLayoutManager(applicationContext, 2)
            adapter = DigitalArtAdapter(dgtArtList, homeActivity)
        }
    }
    private fun getDigitalArt() {
        val dgtart1 = DigitalArt(
            R.drawable.custom_design,
            "Velvet Kat",
            "Wedding Digital Art",
            "Personalize your very own beautiful one of a kind digital wedding portrait that Ill create from one or multiple photos for a memorable personalized wedding gift, invitations, or special decor.",
            "Painting"
        )
        dgtArtList.add(dgtart1)

        val dgtart2 = DigitalArt(
            R.drawable.custom_design2,
            "Unknown Artist Digital Art",
            "Custom Vektor Digital Art",
            "Berikan semua cerita dan kenangan terhadap seseorang dalam satu hadiah",
            "Vektor"
        )
        dgtArtList.add(dgtart2)

        val dgtart3 = DigitalArt(
            R.drawable.custom_design3,
            "Some Random Online Designer Art",
            "Custom Cartoon Digital Art",
            "Personalize your very own digital art photo that full of color like yours life",
            "Cartoon"
        )
        dgtArtList.add(dgtart3)
        val dgtart4 = DigitalArt(
            R.drawable.custom_design4,
            "Some Random Online Designer Art",
            "Watercolor Painting Couple Digital Art",
            "Berbagi kenangan kebahagian bersama orang-orang terpenting didalam hidupmu dalam satu hadiah",
            "Painting"
        )
        dgtArtList.add(dgtart4)

        val dgtart5 = DigitalArt(
            R.drawable.custom_design5,
            "Some Random Online Designer Art",
            "Family Portrait Digital Art",
            "Make A Creative Way to say 'I Love You' for your loved one with a unique gift",
            "Vektor"
        )
        dgtArtList.add(dgtart5)

        val dgtart6 = DigitalArt(
            R.drawable.custom_design6,
            "Some Random Online Designer Art",
            "Custom Minimalist Couple Digital Art",
            "Satukan kenangan-kenangan terindahmu dengan orang-orang tercinta dalam satu hadiah",
            "Vektor"
        )
        dgtArtList.add(dgtart6)

        val dgtart7 = DigitalArt(
            R.drawable.custom_design7,
            "Random Online Designer Art",
            "Best Gift is Memory",
            "Hope You Can Find Your Memorable Design Digital Art As a Gift for your 'Loved One', 'Family', etc.",
            "Vektor"
        )
        dgtArtList.add(dgtart7)
    }

    override fun onClick(digitalArt: DigitalArt) {
        val intent = Intent(applicationContext, DescriptionActivity::class.java)
        intent.putExtra(DGTART_ID_EXTRA, digitalArt.id)
        startActivity(intent)
    }
}