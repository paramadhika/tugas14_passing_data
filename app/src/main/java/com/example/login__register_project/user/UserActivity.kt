package com.example.login__register_project.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.widget.EditText
import android.widget.TextView
import com.example.login__register_project.HomeActivity
import com.example.login__register_project.LoginActivity
import com.example.login__register_project.R
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.login.*

class UserActivity : AppCompatActivity() {
    lateinit var username : TextView
    lateinit var instagram : TextView
    lateinit var fullName : EditText
    lateinit var email : EditText
    lateinit var kontak : EditText
    lateinit var password : EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        username = findViewById(R.id.profil_username)
        instagram = findViewById(R.id.User_insta)
        fullName = findViewById(R.id.full_name)
        email = findViewById(R.id.email)
        kontak = findViewById(R.id.kontak)
        password = findViewById(R.id.pass_akun)


        val bundle = intent.extras
        username.setText(bundle?.getString("username"))
        password.setText(bundle?.getString("pass"))
        password.transformationMethod = PasswordTransformationMethod.getInstance()
        fullName.setText(bundle?.getString("fullname"))
        instagram.setText(bundle?.getString("instagram"))
        kontak.setText(bundle?.getString("kontak"))
        email.setText(bundle?.getString("email"))

        update_user.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("username", username.text.toString())
            bundle.putString("pass", password.text.toString())
            bundle.putString("fullname", fullName.text.toString())
            bundle.putString("instagram", instagram.text.toString())
            bundle.putString("kontak", kontak.text.toString())
            bundle.putString("email", email.text.toString())
            val intent = Intent(this, SetUser::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }
        logOut.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("username", username.text.toString())
            bundle.putString("pass", password.text.toString())
            bundle.putString("fullname", fullName.text.toString())
            bundle.putString("instagram", instagram.text.toString())
            bundle.putString("kontak", kontak.text.toString())
            bundle.putString("email", email.text.toString())
            val intent = Intent(this, LoginActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            finish()
        }

        favoritebtn.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }

        Orderbtn.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }
    }
}


