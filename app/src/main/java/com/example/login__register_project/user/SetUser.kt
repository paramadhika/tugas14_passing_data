package com.example.login__register_project.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.login__register_project.R
import kotlinx.android.synthetic.main.activity_set_user.*

class SetUser : AppCompatActivity() {
    lateinit var username : TextView
    lateinit var fullName : EditText
    lateinit var akunInsta : EditText
    lateinit var email : EditText
    lateinit var kontak : EditText
    lateinit var set_pass : EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_user)

        username = findViewById(R.id.set_username)
        fullName = findViewById(R.id.full_name)
        akunInsta = findViewById(R.id.instagram)
        email = findViewById(R.id.email)
        kontak = findViewById(R.id.kontak)
        set_pass = findViewById(R.id.pass_akun)

        if (intent.extras != null){
            val bundle = intent.extras
            username.setText(bundle?.getString("username"))
            set_pass.setText(bundle?.getString("pass"))
            fullName.setText(bundle?.getString("fullname"))
            akunInsta.setText(bundle?.getString("instagram"))
            kontak.setText(bundle?.getString("kontak"))
            email.setText(bundle?.getString("email"))
        }
        update_user.setOnClickListener{
            intent.putExtra("fullname", fullName.text.toString())
            intent.putExtra("instagram", akunInsta.text.toString())
            intent.putExtra("email", email.text.toString())
            intent.putExtra("kontak", kontak.text.toString())
            intent.putExtra("pass", set_pass.text.toString())

            if (fullName.text.toString() == "" ||
                akunInsta.text.toString() == "" ||
                email.text.toString() == "" ||
                kontak.text.toString() == ""){
                Toast.makeText(this, "Harap Lengkapi Data Registrasi!", Toast.LENGTH_LONG).show()
            }
            else{
                Toast.makeText(this, "Data Berhasil Di Update!", Toast.LENGTH_LONG).show()
                val bundle = Bundle()
                bundle.putString("username", username.text.toString())
                bundle.putString("pass", set_pass.text.toString())
                bundle.putString("fullname", fullName.text.toString())
                bundle.putString("instagram", akunInsta.text.toString())
                bundle.putString("kontak", kontak.text.toString())
                bundle.putString("email", email.text.toString())
                val intent = Intent(this, UserActivity::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
                finish()
            }
        }

    }
}